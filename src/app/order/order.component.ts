import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// Book Service
import { BookService } from '../shared/services/book.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  public totalPrice: number;
  public finalPrice: number;
  public reduction: number;
  public urlOffer: string;
  public cardContent = [];

  constructor(private bookService: BookService, private http: HttpClient) { }



  getTotalPrice(): void {
    this.cardContent = this.bookService.getCardContent();
    let urlInit: string = '';
    let bookPrice: number = 0;
    this.cardContent.forEach( book => {
      for (let i = 0; i < book.quantity; i++) {
        urlInit += book.bookIsbn + ',';
        bookPrice += book.price;
      }
      this.totalPrice = bookPrice;
      this.generateOfferService(urlInit);
    });
  }

  generateOfferService(urlInit: string): void {
    this.urlOffer = 'http://henri-potier.xebia.fr/books/' + urlInit.substring(0, urlInit.length - 1) + '/commercialOffers';
  }
  
  getPriceAfterReduction(): void {
    this.http.get<any>(this.urlOffer).subscribe(offer => {
      this.finalPrice = this.totalPrice - (this.totalPrice * (offer.offers[0].value / 100));
      this.finalPrice = this.finalPrice - offer.offers[1].value;
      if (this.finalPrice >= offer.offers[2].sliceValue) {
        const nbrSlice = this.finalPrice / offer.offers[2].sliceValue;
        this.finalPrice = this.finalPrice - (offer.offers[2].value * Math.floor(nbrSlice));
      }
      this.finalPrice.toFixed(2);
    });
  }

  ngOnInit() {
    this.getTotalPrice()
    this.getPriceAfterReduction()
  }

}
