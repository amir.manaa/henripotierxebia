import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
// Book model
import { Book } from '../models/book.model';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  public bookHttpList = this.http.get('http://henri-potier.xebia.fr/books');

  public bookSelected: BehaviorSubject<Book> = new BehaviorSubject(
    new Book()
  );
  public card = new BehaviorSubject([]);
  public alertBookAdded = new BehaviorSubject(false);

  constructor(private http: HttpClient) {
  }
  getBook(isbn: string) {
    this.bookHttpList.subscribe( (booksList: Book[]) => {
      const bookFromIsbn = booksList.filter( book => {
        return book.isbn === isbn;
      });
      this.bookSelected.next(bookFromIsbn[0]);
      return this.bookSelected;
    });
  }

  addBookToCard(isbn: string, bookTitle: string, bookPrice: number) {
    if (this.card.getValue().length === 0) {
      this.triggerAddNewBook(isbn, bookTitle, bookPrice);
    } else {
      if (this.card.getValue().find( x => x.bookIsbn === isbn)) {
        this.card.getValue().forEach( book => {
          if (book.bookIsbn === isbn) {
            book.quantity = book.quantity + 1;
          }
        });
      } else {
        this.triggerAddNewBook(isbn, bookTitle, bookPrice);
      }
    }
    this.alertBookAdded.next(true);
    setTimeout(() => {
      this.alertBookAdded.next(false);
    }, 1000);
  }
  triggerAddNewBook(isbn: string, bookTitle: string, bookPrice: number) {
    this.card.getValue().push({
      bookIsbn: isbn,
      title: bookTitle,
      price: bookPrice,
      quantity: 1
    });
    this.card.next(this.card.getValue());
  }

  getCardContent() {
    return this.card.value;
  }
}
