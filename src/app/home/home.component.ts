import { Component, OnInit } from '@angular/core';
// BookService
import { BookService } from '../shared/services/book.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public booksList;

  constructor(private bookService: BookService) { }

  // getBooks function
  getBooks() {
    this.bookService.bookHttpList.subscribe( books => {
      this.booksList = books;
    });
  }

  ngOnInit() {
    this.getBooks();
  }

}
