import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
// BookService
import { BookService } from '../../shared/services/book.service';
// Book model
import { Book } from '../../shared/models/book.model';


@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.scss']
})
export class BookDetailComponent implements OnInit {

  public bookSelected;
  public alertBookAdded:boolean = false;
  public show:boolean = false;
  public buttonName:any = 'Affihcer';

  constructor(private bookService: BookService, private activatedRouter: ActivatedRoute) { }

  getBook() {
    this.activatedRouter.params.subscribe( param => {
      this.bookService.getBook(param.isbn);
    });
    this.bookService.bookSelected.subscribe( book => {
      this.bookSelected = book;
      this.show = false;
      this.buttonName = "Afficher";
    });
  }

  addProduct(book: Book) {
    this.bookService.addBookToCard(book.isbn, book.title, book.price);
    // this.bookAdded = true;
    this.bookService.alertBookAdded.subscribe( value => {
      this.alertBookAdded = value;
    });
  }

  toggleSynopsis() {
    this.show = !this.show;
    if(this.show)
      this.buttonName = "Cacher";
    else
      this.buttonName = "Afficher";
  }

  ngOnInit() {
    this.getBook();
  }
}
