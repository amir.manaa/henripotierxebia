import { Component, OnInit } from '@angular/core';
// BookService
import { BookService } from '../../shared/services/book.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent implements OnInit {

  public booksList;
  constructor(private bookService: BookService) {
  }

  // getBooks function
  getBooks() {
    this.bookService.bookHttpList.subscribe( books => {
      this.booksList = books;
    });
  }

  ngOnInit() {
    // call getBooks function
    this.getBooks();
  }
}
