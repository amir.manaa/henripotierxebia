import { Route, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { BooksComponent } from './books-container/books/books.component';
import { BookDetailComponent } from './books-container/book-detail/book-detail.component';
import { OrderComponent } from './order/order.component';

const APP_ROUTE: Route[] = [
    { path: 'home', component: HomeComponent },
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'books', component: BooksComponent, children: [
        { path: ':isbn', component: BookDetailComponent },
    ]},
    { path: 'order', component: OrderComponent }
];

export const AppRouting =  RouterModule.forRoot(APP_ROUTE);
